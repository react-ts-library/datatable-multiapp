import React, { useState } from 'react';
import Amount from './hooks/Amount';
import Navigator from './hooks/Navigator';
import Search from './hooks/Search';
import Table from './hooks/Table';
import parseDataFn from './functions/parserDataFn';
import { IDataTable, IFunctionality } from './interface/IDataTable';
import './scss/dataTable.scss';


const updateAll = (todo: IFunctionality, fieldsToUpdate: Partial<IFunctionality>) => {
    return { ...todo, ...fieldsToUpdate };
}

const defaultFunctionality: IFunctionality = {
    amount: true,
    search: true,
    filter: true
};


const DataTable = (params: IDataTable) => {

    const { callback, title, data, headers, pagination, config, options = [], color, functionality } = params;

    if (color) {
        document.documentElement.style.setProperty('--color', color)
    }

    // console.log(params.functionality)

    const allfunctionality = updateAll(defaultFunctionality, functionality);

    // console.log("todo2")

    let value2: any;
    value2 = config

    const [disabled, setDisabled] = useState(false);

    const [amount, setAmount] = useState(15);
    const [search, setSearch] = useState(false);
    const [query, setQuery] = useState('');
    const [filter, setFilter] = useState(false);
    const [noData, setNodata] = useState<boolean>(false);

    const headers_data = options.length ? [...headers, { column_key: 'options', column_name: 'Opciones', column_type: 'options' }] : headers;

    const parsed_data = parseDataFn(data, headers, options);

    const callbackAmount = async (num: number) => {
        const resp = {
            documents: num,
            query: search ? query : '',
            page: 1,
            filter: filter
        }
        setAmount(num);
        setDisabled(true);
        await callback(resp);
        setDisabled(false);
    };

    const callbackSearch = async (q: string, s: boolean) => {
        const resp = {
            documents: amount,
            query: s ? q : '',
            page: 1,
            filter: filter
        }
        setSearch(s);
        setQuery(q);
        setDisabled(true);
        await callback(resp);
        setDisabled(false);
    };

    const callbackFilter = async (f: boolean) => {
        const resp = {
            documents: amount,
            query: search ? query : '',
            page: 1,
            filter: f
        };
        setFilter(f);
        setDisabled(true);
        await callback(resp);
        setDisabled(false);
    };

    const callbackNavigator = async (p: number) => {
        const resp = {
            documents: amount,
            query: search ? query : '',
            page: p,
            filter: filter
        }
        setDisabled(true);
        await callback(resp);
        setDisabled(false);
    };

    return (
        <div className="componentDataTable">
            <div className="sepAmoSea">
                {
                    allfunctionality.amount &&
                    <Amount
                        disabled={disabled}
                        callbackAmount={callbackAmount}
                    />
                }
                <Search
                    title={title}
                    disabled={disabled}
                    setNodata={setNodata}
                    callbackSearch={callbackSearch}
                    callbackFilter={callbackFilter}
                    allfunctionality={allfunctionality}
                />
            </div>
            <Navigator
                pagination={pagination}
                disabled={disabled}
                callbackNavigator={callbackNavigator}
                setDisabled={setDisabled}
                amount={amount}
            />
            <Table
                data={parsed_data}
                noData={noData}
                options={options}
                headers={headers_data}
                disabled={disabled}
                setDisabled={setDisabled}
            />
        </div>
    )
}

export default DataTable


