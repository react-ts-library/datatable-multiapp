import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';

const TableRow = ({ row: { parsed_data, original_data }, headers, options, F_options, number, position }: any) => {

    let value1: any;
    value1 = options

    const { showOpt, selectOpt } = F_options

    const data = Object.keys(parsed_data).map((key) => parsed_data[key]);

    const getExtention = (url: string) => url.split('.').pop();

    const downloadFile = (url: string) => {
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        a.click();
    };

    return (
        <>
            {(headers[number].column_type === 'image') ?
                <div className="imageName">
                    <img src={data[number]} alt={data[number]} className="imgPhoto" />
                </div> :
                (headers[number].column_type === 'file') ?
                    <div
                        className="spaceFile"
                        onClick={() => downloadFile(data[number])}
                        style={{ cursor: 'pointer' }}
                        title={''}
                    >
                        <div className={`file ${getExtention(data[number])}`}>
                            <span>{getExtention(data[number])}</span>
                            <i className="fas fa-cloud-download-alt"></i>
                        </div>
                        <div className="corner" />
                    </div> :
                    (headers[number].column_type === 'options') ?
                        <div className="action">
                            <div onClick={() => selectOpt(position)}>
                                <FontAwesomeIcon icon={faEllipsisH} style={{ cursor: 'pointer' }} />
                            </div>
                            <div className={`select ${showOpt === position ? 'show' : ''}`}
                                onMouseLeave={() => selectOpt('')}
                            >
                                {
                                    data[number].map((opt: any, k: number) =>
                                        <p key={k} onClick={() => { opt.onClick(original_data); selectOpt('') }} >{opt.label}</p>)
                                }
                            </div>
                        </div>
                        :
                        <p>
                            {data[number]}
                        </p>

            }
        </>
    )
}

export default TableRow
