import { IDataTableHeader } from '../interface/IDataTable';

const getValidData = (header: IDataTableHeader, data: any) => {

    const isOk = typeof data[header.column_key] === 'undefined';

    let key: any;

    switch (header.column_type) {

        case 'image':
            if (isOk) {
                key = 'https://res.cloudinary.com/cristoper/image/upload/v1624510024/librarys/default-image_oloij6.png';
            } else {
                key = data[header.column_key];
            }
            break;

        // case 'options':
        //     key = data[header.column_key];
        //     break;

        case 'text':
            if (isOk) {
                key = '---';
            } else {
                key = data[header.column_key];
            }
            break;

        case 'file':
            if (isOk) {
                key = 'nothing.zzz';
            } else {
                key = data[header.column_key];
            }
            break;

        default:
            break;
    }

    return { [header.column_key]: key };

}

export default function parseDataFn(data: any, headers: IDataTableHeader[], options: any[]) {

    const result = data.map((dt: any) => {
        let obj: any = {};
        headers.map((hd) => { return obj = { ...obj, ...getValidData(hd, dt) } });
        let res: any;
        // if( options.length ){ 
        res = { parsed_data: { ...obj, options }, original_data: dt }
        // } else {
        //     res = { parsed_data: obj, original_data: dt }
        // }
        return res;
    });
    return result;
}