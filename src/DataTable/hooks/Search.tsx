import React, { useState } from 'react'
import '../scss/search.scss'
import ReactTooltip from 'react-tooltip'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faFilter, faTimes } from '@fortawesome/free-solid-svg-icons'
import { IFunctionality } from '../interface/IDataTable'

interface IProps {
    setNodata: React.Dispatch<React.SetStateAction<boolean>>,
    title: string,
    disabled: boolean
    callbackSearch: Function
    callbackFilter: Function
    allfunctionality: IFunctionality
}

const Search = ({ setNodata, callbackSearch, callbackFilter, disabled, title, allfunctionality }: IProps) => {

    const [optSearch, setOptSearch] = useState(false);

    const [search, setSearch] = useState(false);
    const [query, setQuery] = useState('');
    const [filter, setFilter] = useState(false);


    // console.log(allFunctionality)

    const onSetQuey = (e: any) => setQuery(e.target.value);

    const Submit = (e: any) => {
        e.preventDefault();
        onSearch()
    }

    const onSearch = async () => {
        setSearch(true);
        setNodata(true);
        await callbackSearch(query, true);
    };

    const onClear = async (e: any) => {
        e.preventDefault();
        setSearch(false);
        setNodata(false)
        setQuery('');
        await callbackSearch(query, false);
    };

    const onFilter = async (f: any) => {
        setFilter(!f);
        await callbackFilter(!f);
    };

    return (
        <div className="contTirepN">
            <div className="nameRep">
                {title}
            </div>
            <div className="icRep">
                {
                    allfunctionality.search &&
                    <div className="firstdiv">
                        {
                            <div className="contInp" style={{ display: `${optSearch ? 'flex' : 'none'}` }}>
                                <label>
                                    <form onSubmit={search ? onClear : Submit}>
                                        <input
                                            value={query}
                                            name="asd"
                                            onChange={onSetQuey}
                                            type="text"
                                            placeholder="Buscar ..."
                                            disabled={search || disabled}
                                            required
                                        />
                                        <button type="submit" disabled={disabled}>
                                            {
                                                search ? 'Borrar' : 'Buscar'
                                            }
                                        </button>
                                    </form>
                                </label>
                                <button
                                    style={{ color: '#000' }}
                                    className="btnclose"
                                    onClick={() => setOptSearch(false)}
                                    disabled={disabled}
                                    data-tip="Cerrar"
                                >
                                    <FontAwesomeIcon icon={faTimes} />
                                </button>
                            </div>
                        }
                        {
                            <button
                                style={{ display: `${optSearch ? 'none' : 'block'}`, color: '#000' }}
                                onClick={() => setOptSearch(true)}
                                disabled={disabled}
                                data-tip="Buscar"
                            >
                                <FontAwesomeIcon icon={faSearch} />
                            </button>
                        }
                    </div>
                }
                {
                    allfunctionality.filter &&
                    <button
                        style={{ color: `${filter ? 'var(--color)' : ''}` }}
                        onClick={() => onFilter(filter)}
                        disabled={disabled}
                        data-tip="Filtrar"
                    >
                        <FontAwesomeIcon icon={faFilter} />
                    </button>
                }
            </div>
            <ReactTooltip className="tolltip" effect='solid' place="top" />
        </div>
    )
}

// Search.defaultProps = {
//     functionality: {
//         amount: true,
//         search: true,
//         filter: true
//     }
// }

export default Search
