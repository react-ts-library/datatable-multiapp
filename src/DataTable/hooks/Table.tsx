import React, { useEffect, useState } from 'react'
import TableRow from '../components/TableRow';
import { IDataTableHeader } from '../interface/IDataTable';

import '../scss/table.scss'

interface IProps {
    data: any[],
    noData: boolean,
    headers: IDataTableHeader[]
    disabled: boolean
    setDisabled: Function,
    options?: any[]
}

const Table = ({ data, noData, headers, disabled, setDisabled, options }: IProps) => {

    let value1: any, value2: any;
    value1 = disabled
    value2 = setDisabled

    const [showOpt, setshowOpt] = useState<string>('')

    const selectOpt = (id: string) => {
        if (id === showOpt) {
            setshowOpt('')
        } else {
            setshowOpt(id)
        }
    }
    useEffect(() => {
        setshowOpt('')
    }, [data])

    return (
        <div>
            {
                !data.length ? <div className="ntnodata">{noData ? 'Sin resultados para su búsqueda' : 'Sin registros'}</div> :
                    <div className="tableContent">
                        <div className="containterColum">
                            <div className="titleColum">
                                {
                                    headers[0].column_name
                                }
                            </div>
                            <div className="dateColum">
                                {
                                    data.map((dt: any, key: number) =>
                                        <div className="item" key={key}>
                                            <TableRow
                                                row={dt}
                                                options={options}
                                                F_options={{ showOpt, selectOpt }}
                                                headers={headers}
                                                number={0}
                                            />
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                        <div className="allContainerColumns">
                            {
                                Array.from(Array(headers.length - 1).keys()).map((index: number) =>
                                    <div className="containterColum" key={index}>
                                        <div className="titleColum">
                                            {
                                                headers[index + 1].column_name
                                            }
                                        </div>
                                        <div className="dateColum">
                                            {
                                                data.map((dt: any, key: number) =>
                                                    <div className="item" key={key}>
                                                        <TableRow
                                                            row={dt}
                                                            options={options}
                                                            F_options={{ showOpt, selectOpt }}
                                                            headers={headers}
                                                            type={2}
                                                            number={index + 1}
                                                            position={key}
                                                        />
                                                    </div>
                                                )
                                            }

                                        </div>
                                    </div>
                                )
                            }
                        </div>
                    </div>
            }
        </div>
    )
}

export default Table
