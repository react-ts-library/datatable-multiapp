import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import '../scss/navigator.scss'
interface IProps {

    pagination: {
        success: boolean;
        actualPage: number;
        totalPages: number;
        hasPrevPage: boolean;
        hasNextPage: boolean;
        itemCount: number;
        nextPage: number;
        total: number;
    }
    disabled: boolean
    callbackNavigator: Function
    setDisabled: Function
    amount: number
}

const Navigator = ({ pagination, disabled, setDisabled, callbackNavigator, amount }: IProps) => {

    // console.log(disabled, setDisabled)
    let value1: any, value2: any;
    value1 = disabled;
    value2 = setDisabled;

    const {
        hasNextPage,
        hasPrevPage,
        actualPage,
        itemCount,
        total,
        totalPages,
        success
    } = pagination;

    // const styles = { display: 'inline-block', padding: '7px' };
    //const selected = { backgroundColor: 'yellow' }

    const onNavigate = async (pag: number) => {
        if (pag === pagination.actualPage) return
        // console.log("object")
        await callbackNavigator(pag);
    };

    const getButtonArr = (tot: number, init: number) =>
        Array.from(Array(tot).keys()).map((key: number) =>
            <button key={key} className={`number ${(init + key === actualPage) ? 'active' : ''}`}
                onClick={() => onNavigate(init + key)}
            >{init + key}</button>
        );

    const generatePaginationButtons = () => {

        let buttons;

        if (totalPages <= 12) {
            buttons = getButtonArr(totalPages, 1);
        } else {
            if (actualPage <= 5) {
                buttons = [
                    getButtonArr(7, 1),
                    <div className="number" key={'dotsFinish'}>...</div>,
                    <button className="number" onClick={() => onNavigate(totalPages)} key={'last'} >{totalPages}</button>
                ]
            } else if (actualPage >= totalPages - 5) {
                buttons = [
                    <button className="number" onClick={() => onNavigate(1)} key={'first'} >1</button>,
                    <div className="number" key={'dotsInitial'}>...</div>,
                    getButtonArr(7, totalPages - 6)
                ]
            } else {
                buttons = [
                    <button className="number" key={'first'} onClick={() => onNavigate(1)} >1</button>,
                    <div className="number" key={'dotsInitial'}>...</div>,
                    <button className="number" onClick={() => onNavigate(actualPage - 2)} key={actualPage - 2} >{actualPage - 2}</button>,
                    <button className="number" onClick={() => onNavigate(actualPage - 1)} key={actualPage - 1} >{actualPage - 1}</button>,
                    <button className="number active" onClick={() => onNavigate(actualPage)} key={actualPage} >{actualPage}</button>,
                    <button className="number" onClick={() => onNavigate(actualPage + 1)} key={actualPage + 1} >{actualPage + 1}</button>,
                    <button className="number" onClick={() => onNavigate(actualPage + 2)} key={actualPage + 2} >{actualPage + 2}</button>,
                    <div className="number" key={'dotsFinish'}>...</div>,
                    <button className="number" key={'last'} onClick={() => onNavigate(totalPages)} >{totalPages}</button>,
                ]
            }
        }

        return buttons;

    }

    return (
        <>
            {
                success &&
                <div className="headNavegate">
                    <div className="detailPg">
                        {/* Mostrando {itemCount} registro en la pagina {actualPage} de {total} */}
                        Mostrando <span>{amount * (actualPage - 1) + 1} - {itemCount === amount ? amount * actualPage : amount * (actualPage - 1) + itemCount}</span> de <span>{total}</span>
                    </div>
                    <div className="pagination">
                        <button className="itemPag" onClick={() => onNavigate(actualPage - 1)} disabled={!hasPrevPage}>
                            <FontAwesomeIcon icon={faChevronLeft} style={{ color: 'var(--color)' }} />
                        </button>
                        <div className="showNumber">
                            {
                                generatePaginationButtons()
                            }
                        </div>
                        <button className="itemPag" onClick={() => onNavigate(actualPage + 1)} disabled={!hasNextPage}>
                            <FontAwesomeIcon icon={faChevronRight} style={{ color: 'var(--color)' }} />
                        </button>
                    </div>
                </div>
            }
        </>
    );

}

export default Navigator
