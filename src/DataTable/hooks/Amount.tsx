import React from 'react';
interface IProps {
    disabled: boolean
    callbackAmount: Function
}

const Amount = ({ disabled, callbackAmount }: IProps) => {

    const onSelect = (e: any) => callbackAmount(e.target.value)

    return (
        <div>
            <select className="selectcount" onChange={onSelect} disabled={disabled}>
                <option value={15}>15</option>
                <option value={30}>30</option>
                <option value={50}>50</option>
            </select>
        </div>
    )
}

export default Amount
