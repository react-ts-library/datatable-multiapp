import React, { useState } from "react";
import DataTable from "./DataTable";

export default {
    title: "DataTable"
};


export const PruebaInicial = () => {

    const newData: any = [
        {
            alterado_por: "sebastian enseña",
            fecha_creacion: "22/06/2021 - 09:09:38",
            idmaterial: "60d1ef22778d7100128ac6ad",
            nombre_archivo: "prueba",
            peso_archivo: "53.36 kb",
            ultima_edicion: "22/06/2021 - 09:09:38",
            url_archivo: "https://playtecacademy.s3.amazonaws.com/entity/undefined/files/1623965824476-prueba"
        },
        {
            alterado_por: "sebastian enseña",
            fecha_creacion: "22/06/2021 - 14:31:30",
            idmaterial: "60d23a92778d7100128ac9de",
            nombre_archivo: "prueba imagen 1",
            peso_archivo: "219.57 kb",
            ultima_edicion: "22/06/2021 - 14:31:30",
            url_archivo: "https://playtecacademy.s3.us-east-2.amazonaws.com/entity/undefined/files/1623965824476-prueba%20imagen%201"
        },
        {
            alterado_por: "sebastian enseña",
            fecha_creacion: "22/06/2021 - 14:31:30",
            idmaterial: "60d23a92778d7100128ac9de",
            nombre_archivo: "prueba imagen 1",
            peso_archivo: "219.57 kb",
            ultima_edicion: "22/06/2021 - 14:31:30",
            url_archivo: "https://playtecacademy.s3.us-east-2.amazonaws.com/entity/undefined/files/1623965824476-prueba%20imagen%201"
        },
        {
            alterado_por: "sebastian enseña",
            fecha_creacion: "22/06/2021 - 14:31:30",
            idmaterial: "60d23a92778d7100128ac9de",
            nombre_archivo: "prueba imagen 1",
            peso_archivo: "219.57 kb",
            ultima_edicion: "22/06/2021 - 14:31:30",
            url_archivo: "https://playtecacademy.s3.us-east-2.amazonaws.com/entity/undefined/files/1623965824476-prueba%20imagen%201"
        }
    ]

    const pag: any = {
        actualPage: 1,
        hasNextPage: false,
        hasPrevPage: false,
        itemCount: 5,
        success: true,
        total: 5,
        totalPages: 12
    };

    const detailMaterial = (obj: any) => {
        console.log('EL OBJETO QUE RECIBO: ', obj)
    }

    const options = [
        {
            label: 'Ver Material',
            onClick: detailMaterial
        }
    ]

    const head = [
        {
            column_key: 'url_archivo',
            column_name: 'Archivo',
            column_type: 'text'
        },
        {
            column_key: 'fecha_creacion',
            column_name: 'Fecha de creación',
            column_type: 'text'
        },
        {
            column_key: 'nombre_archivo',
            column_name: 'Nombre del Archivo',
            column_type: 'text'
        },
        {
            column_key: 'alterado_por',
            column_name: 'Modificado por',
            column_type: 'text'
        },
        {
            column_key: 'peso_archivo',
            column_name: 'Peso del archivo',
            column_type: 'text'
        }
    ];

    const getMaterialData = (dt: any) => {
        const { query } = dt;
    }

    return (
        <>
            <DataTable
                color="red"
                data={newData}
                title='Materiales'
                callback={getMaterialData}
                pagination={pag}
                options={options}
                headers={head}
                functionality={{
                    amount: false, filter: false, 
                }}
            />
        </>

    );
}
