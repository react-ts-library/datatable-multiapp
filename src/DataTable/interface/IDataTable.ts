export interface IDataTable {
    functionality?: IFunctionality
    color?: string
    title: string,
    headers: IDataTableHeader[],
    data: any[],
    options?: IDataTableOption[],
    callback: Function,
    pagination: {
        success: boolean,
        actualPage: number,
        totalPages: number,
        hasPrevPage: boolean,
        hasNextPage: boolean,
        // Lo que te llega :v
        itemCount: number,
        nextPage: number,
        // todos los docs en total (100,200)
        total: number
    },
    config?: IDataTableConfiguration

}
export interface IFunctionality {
    amount?: boolean
    search?: boolean
    filter?: boolean
}

export interface IDataTableHeader {
    column_key: string,
    column_name: string,
    column_type: string // text | image | file | options
}

interface IDataTableConfiguration {

    // Que sí,muestra la por defecto | Que nó, deja en blanco | Si manda string, esta mandando la imagen
    show_default_image?: boolean | string,
    show_index_column?: boolean
    // event_type: 'on_click_item'
    activate_on_click_item?: boolean

}

// Si el log esta activo, que se muestre una imagen de error cuando no funque la tabla. Si esta activo que se muestren los logs nomas.

export interface IDataTableOption {
    label: string,
    onClick: Function
}
