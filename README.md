# datatable-multiapps

## Installation

```sh
npm install datatable-multiapps
```

or

```sh
yarn add datatable-multiapps
```

## Usage

### Using NPM

1 . Require datatable-multiapps after installation

```js
import DataTable from 'datatable-multiapps';
```

2 . Include datatable-multiapps component with the props

```jsx
<DataTable
    title=''
    data={[]}
    headers={}
    pagination={}
    callback={}
    options={}
    color=""
    functionality={{}}
/>
```

## Example

```tsx
import DataTable from 'datatable-multiapps';

export const ExamplePage = ():JSX.Element => {

    const newData: any = [
        {
            alterado_por: "sebastian enseña",
            fecha_creacion: "22/06/2021 - 09:09:38",
            idmaterial: "60d1ef22778d7100128ac6ad",
            nombre_archivo: "prueba",
            peso_archivo: "53.36 kb",
            ultima_edicion: "22/06/2021 - 09:09:38",
            url_archivo: "https://datatable-multiapps.example.com/example01"
        },
        {
            alterado_por: "sebastian no enseña",
            fecha_creacion: "22/06/2021 - 14:31:30",
            idmaterial: "60d23a92778d7100128ac9de",
            nombre_archivo: "prueba imagen 1",
            peso_archivo: "219.57 kb",
            ultima_edicion: "22/06/2021 - 14:31:30",
            url_archivo: "https://datatable-multiapps.example.com/example02"
        }
    ];

    const pag: any = {
        actualPage: 1,
        hasNextPage: false,
        hasPrevPage: false,
        itemCount: 2,
        success: true,
        total: 2,
        totalPages: 1
    };

    const detailMaterial = (obj: any) => {
        console.log('received object: ', obj)
    }

    const options:any = [
        {
            label: 'Ver Material',
            onClick: detailMaterial
        }
    ]

    const head:any = [
        {
            column_key: 'url_archivo',
            column_name: 'Archivo',
            column_type: 'text'
        },
        {
            column_key: 'fecha_creacion',
            column_name: 'Fecha de creación',
            column_type: 'text'
        },
        {
            column_key: 'nombre_archivo',
            column_name: 'Nombre del Archivo',
            column_type: 'text'
        },
        {
            column_key: 'alterado_por',
            column_name: 'Modificado por',
            column_type: 'text'
        },
        {
            column_key: 'peso_archivo',
            column_name: 'Peso del archivo',
            column_type: 'text'
        }
    ];

    const getMaterialData = (dt: any) => {
        const { query } = dt;
        console.log(query);
    }

    return (
        <>
            <DataTable
                title='Materiales'
                data={newData}
                headers={head}
                pagination={pag}
                callback={getMaterialData}
                options={options}
                color="red"
                functionality={{
                    amount: false, 
                    filter: false, 
                    search: false
                }}
            />
        </>

    );
}
```

## Options

Note:

- The default `color` inside the DataTable component is "dark".
- In the `functionality` option you only need to add the parameter that you do not want to show in "false" value, the other parameters are optional and have a "true" value by default. 

| Global            | Status    | Type      |            
| :---------------- | :-------- | :-------- | 
| title             | required  | String    |                   
| data              | required  | any[]     |                   
| headers           | required  | any[]     |                   
| pagination        | required  | Object    |                   
| callback          | required  | Func      |                   
| options           | optional  | any[]     |                   
| color             | optional  | String    |    
| functionality     | optional  | Object    |                   

## License

MIT